var that;
class Tab {
  constructor(id) {
    that = this;
    // 获取元素
    this.Tab = document.querySelector(id);
    // 获取添加按钮元素
    this.add = this.Tab.querySelector(".tabadd");
    //  获取li的父元素
    this.ul = this.Tab.querySelector(".fisrstnav> ul:first-child");
    // 获取tab栏下部分父元素
    this.tabscon = this.Tab.querySelector(".tabscon");
    this.init();
  }
  // init 初始化操作让相关的元素绑定事件
  init() {
    //  更新节点
    this.updateNode();
    this.add.onclick = this.AddTab;
    for (var i = 0; i < this.lis.length; i++) {
      this.lis[i].index = i;
      this.lis[i].onclick = this.ToggleTab;
      this.removes[i].onclick = this.removeTab;
      this.spans[i].ondblclick = this.editTab;
      this.sections[i].ondblclick = this.editTab;
    }
  }
  // 因为我们动态添加元素 需要从新获取对应的元素
  updateNode() {
    this.lis = this.Tab.querySelectorAll("li");
    this.sections = this.Tab.querySelectorAll("section");
    this.removes = this.Tab.querySelectorAll(".icon-guanbi");
    this.spans = this.Tab.querySelectorAll(".fisrstnav  li > span:first-child");
  }
  // 排他思想 清除样式
  clearClass() {
    for (var i = 0; i < this.lis.length; i++) {
      this.lis[i].className = "";
      this.sections[i].className = "";
    }
  }
  // 切换功能
  ToggleTab() {
    that.clearClass();
    this.className = "liactive";
    that.sections[this.index].className = "conactive";
  }
  // 添加功能
  AddTab() {
    that.clearClass();
    // 获取随机数字
    var random = Math.random();
    // 创建li 和section 元素
    var li = `<li class="liactive"><span>新建测试</span><span class="iconfont icon-guanbi"></span></li>`;
    var section = ` <section class = 'conactive'>${random}</section>`;
    // 添加父元素里面
    that.ul.insertAdjacentHTML("beforeend", li);
    that.tabscon.insertAdjacentHTML("beforeend", section);
    // 重置事件
    that.init();
  }

  //  删除功能
  removeTab(e) {
    e.stopPropagation(); // 阻止冒泡 防止的触发li的 点击事件
    // 获取remove删除按钮的父亲节点的索引号
    var index = this.parentNode.index;
    // 删除被点击的按钮对应的li 和 section
    that.lis[index].remove()
    that.sections[index].remove()
    // 重置事件
    that.init()
    // 再删除没有选中状态按钮时候 其他选中状态的li 还是选中状态
    if (document.querySelector('.liactive')) return
    index--
    // 再删除第一个按钮被他选中状态的话 那么新的第一项默认选中状态 
    // 短路运算符 如果第一项满足那么就执行第二项 否则短路第二项不执行
    if (index < 0) that.lis[0] && that.lis[0].click()
    // 在删除点击被选中按钮时候  使它前一个li变成选中状态
    that.lis[index] && that.lis[index].click()
  }

  // 编辑功能
  editTab() {
    // 将双击他们的文本赋值给变量str
    var str = this.innerHTML
    // 禁止双击选中文字
    window.getSelection ? window.getSelection().removeAllRanges() : document.selection.empty();
    // 在span 和 section里面的添加 文本输入框
    this.innerHTML = '<input type = "text">';
    // li下面有2个span  选取他的第一个孩子 span  
    var input = this.children[0]
    // 将之前标签文本放到文本输入框中
    input.value = str
    input.select(); // 文本框的里面的文字处于选定状态
    // 设置一个离开光标事件 将本文输入框赋值给 span
    input.onblur = function () {
       this.parentNode.innerHTML = this.value
    }
    // 设置一个键盘弹起事件
    input.onkeyup = function (e) {
      if(e.keyCode === 13) return this.blur()
    }
  }
}
new Tab("#tab");
